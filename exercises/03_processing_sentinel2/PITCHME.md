---?image=assets/img/grass.png&position=bottom&size=100% 30%
@title[Front page]

@snap[north span-100]
<br>
## Procesamiento digital de imágenes satelitales y SIG
@snapend

@snap[south message-box-white]
Dra. Verónica Andreo<br>CONICET<br><br>
@img[width=600px](assets/img/LogoIG_CONAE_UNC.png)<br>
@snapend

---?image=assets/img/grass.png&position=bottom&size=100% 30%

## Ejercicio: Trabajamos con imágenes Sentinel 2 
<br>
@fa[satellite text-gray fa-3x]

---

@snap[north-west span-60]
### Contenidos
@snapend

@snap[west span-100]
<br><br>
@ol[list-content-verbose]
- Listado y descarga de escenas Sentinel 2
- Importar datos Sentinel 2 a GRASS GIS
- Balance de colores
- Pre-procesamiento
- Detección de nubes y sombras
- Índices de agua y vegetación
- Segmentación
- Classificación supervisada 
@olend
@snapend

---

@snap[north span-100]
### Datos Sentinel 2
@snapend

@snap[west span-40]
![Sentinel 2 satellite](assets/img/sentinel2.jpg)
@snapend

@snap[east span-60]
<br><br>
@ul[list-content-verbose](false)
- Lanzamiento: Sentinel-2A en 2015, Sentinel-2B en 2017
- Tiempo de revisita: 5 dias
- Cobertura sistemática de áreas terrestres y costeras entre los 84°N y 56°S
- 13 bandas espectrales con resolución espacial de 10 m (VIS y NIR), 20 m (red-edge y SWIR) y 60 m (otras)
@ulend
@snapend

+++

![Sentinels](assets/img/sentinel_satellites.jpg)

@size[22px](ESA - Satélites Copernicus Sentinel. Más información en: https://www.copernicus.eu/en/about-copernicus/infrastructure/discover-our-satellites)

+++

![Sentinel and Landsat bands](assets/img/landsat_and_sentinel_bands.png)

@size[28px](Distribución de bandas de Sentinel 2 comparadas con Landsat)

---

### @fa[plug text-green] Extensiones para datos Sentinel @fa[plug text-green]

- [i.sentinel.download](https://grass.osgeo.org/grass7/manuals/addons/i.sentinel.download.html): descarga productos Copernicus Sentinel de Copernicus Open Access Hub
- [i.sentinel.import](https://grass.osgeo.org/grass7/manuals/addons/i.sentinel.import.html): importa datos Sentinel descargados de Copernicus Open Access Hub
- [i.sentinel.preproc](https://grass.osgeo.org/grass7/manuals/addons/i.sentinel.preproc.html): importa y realiza corrección atmosférica y topográfica de imágenes S2
- [i.sentinel.mask](https://grass.osgeo.org/grass7/manuals/addons/i.sentinel.mask.html): crea máscaras de nubes y sombras para imágenes S2

@size[24px](Ver <a href="https://grasswiki.osgeo.org/wiki/SENTINEL">Sentinel wiki</a> para más detalles)

+++

- Para conectarse al [Copernicus Open Access Hub](https://scihub.copernicus.eu/) a través de [i.sentinel.download](https://grass.osgeo.org/grass7/manuals/addons/i.sentinel.download.html), se necesita ser usuario [registrado](https://scihub.copernicus.eu/dhus/#/self-registration)
- Crear el archivo *`SETTING_SENTINEL`* en el directorio *`$HOME/gisdata/`* con el siguiente contenido:

```
your_username
your_password
```

+++

@fa[download fa-2x text-green] Descargar el [código](https://gitlab.com/veroandreo/grass-gis-conae/raw/master/code/04_S2_imagery_code.sh?inline=false) para seguir esta sesión

---?code=code/04_S2_imagery_code.sh&lang=bash&title=Descargar datos Sentinel 2

@[22-23](Iniciar GRASS GIS en el location NC y crear un nuevo mapset)            
@[25-26](Instalar la extensión i.sentinel)
@[28-29](Definir la región computacional)
@[31-40](Lista de escenas disponibles que intersectan la región computacional)
@[42-47](Lista de escenas disponibles que contienen la región computacional)
@[49-52](Descargar la escena seleccionada)

+++

Como la descarga desde el Copernicus Open Access Hub toma un tiempo, nos lo saltaremos
<br><br>

@fa[download fa-2x text-green] Descargar la escena [Sentinel 2](https://www.dropbox.com/s/2k8wg9i05mqgnf1/S2A_MSIL1C_20180822T155901_N0206_R097_T17SQV_20180822T212023.zip?dl=0) pre-descargada que usaremos y moverla a *`HOME/gisdata`*
<br>

---?code=code/04_S2_imagery_code.sh&lang=bash&title=Importar datos Sentinel 2

@[54-68](Imprimir información sobre las bandas antes de importarlas)
@[70-73](Importar los datos - Nos saltaremos este paso por ahora)

---

#### Niveles de procesamiento en datos Sentinel 2

- **L1C**: Reflectancia a tope de atmósfera o Top of Atmosphere (TOA). Desde el lanzamiento hasta 2019. 
- **L2A**: Reflectancia Superficial o Bottom of Atmosphere (BOA), i.e., los datos han sido corregidos para remover los efectos de la atmósfera. Desde 2019 en adelante.

> Reprocesamiento de datos de archivo??

---

@snap[north span-100]
#### Importar con corrección atmosférica: [i.sentinel.preproc](https://grass.osgeo.org/grass7/manuals/addons/i.sentinel.preproc.html)
@snapend

@snap[west span-60]
<br>
![](https://grass.osgeo.org/grass76/manuals/addons/i_sentinel_preproc_GWF.png)
@snapend

@snap[east span-40]
@ol[list-content-verbose](false)
- descomprimir el archivo S2 que descargamos
- mapa de visibilidad o AOD (Aerosol Optic Depth)
- mapa de elevación
@olend
@snapend

+++

@snap[north span-100]
Obtener AOD de [http://aeronet.gsfc.nasa.gov](https://aeronet.gsfc.nasa.gov)
@snapend

@snap[west span-40]
<img src="assets/img/aeronet_download.png" width="65%">
@snapend

@snap[east span-60]
@ul[list-content-verbose](false)
- Estación EPA-Res_Triangle_Pk
- Seleccionar fechas de inicio y final
- Seleccionar: *`Combined file`* y *`All points`*
- Descargar y descomprimir en *`$HOME/gisdata`* (el archivo final tiene extensión .dubovik)
@ulend

Si eso no funciona, aquí está el [archivo AOD](https://gitlab.com/veroandreo/curso-grass-gis-rioiv/blob/master/data/180819_180825_EPA-Res_Triangle_Pk.zip)
@snapend

+++

@fa[mountain text-green] Mapa de elevación @fa[mountain text-green] 
<br>

Por ahora, usaremos el mapa de `elevation` presente en NC
<br>

...pero sólo la región cubierta por el mapa de elevación será corregida atmosféricamente... @fa[exclamation text-orange] 

<br>

> @size[24px](Para obtener los DEMs de SRTM para su región directamente en GRASS GIS, vea la extensión [r.in.srtm.region](https://grass.osgeo.org/grass76/manuals/addons/r.in.srtm.region.html))

+++

Importar con corrección atmosférica
@code[bash](code/04_S2_imagery_code.sh)

@[98-100](Entrar en el directorio con la escena de Sentinel 2 y descomprimir el archivo)
@[109-115](Ejecutar *i.sentinel.preproc* usando el mapa de elevación de NC)
@[117-120](Mejorar la visualización de colores)
@[122-126](Mostrar mapa RGB corregido atmosféricamente)

+++

@img[span-80](assets/img/S2_color_enhance_corr_full_elev_region.png)

@size[24px](Escena Sentinel 2 con colores balanceados - Composición RGB 321)

---

Máscara de nubes y sombras de nubes
@code[bash](code/04_S2_imagery_code.sh)

@[189-193](Identificar y enmascarar nubes y sus sombras)
@[195-201](Visualización de la salida)

+++

![Clouds and cloud shadows](assets/img/S2_clouds_and_shadows.png)

@size[24px](Nubes y sombras de nubes identificadas por [i.sentinel.mask](https://grass.osgeo.org/grass7/manuals/addons/i.sentinel.mask.html))

---

### Índices de agua y vegetación
@code[bash](code/04_S2_imagery_code.sh)

@[209-223](Definir región computacional)
@[225-229](Establecer máscara)
@[231-239](Estimación de los índices de vegetación)
@[241-242](Instalar extensión *i.wi*)
@[244-247](Estimación de índice de agua)

+++

![Sentinel 2 - NDVI and EVI](assets/img/S2_ndvi_evi.png)

@size[24px](NDVI y EVI de Sentinel 2)

+++

<img src="assets/img/S2_ndwi.png" width="60%">

@size[24px](NDWI de Sentinel 2)

---

### Segmentación
@code[bash](code/04_S2_imagery_code.sh)

@[255-256](Instalar la extensión *i.superpixels.slic*)
@[258-261](Listar los mapas y crear grupos y subgrupos)
@[263-266](Ejecutar i.superpixels.slic)
@[268-271](Ejecutar i.segment)
@[273-277](Mostrar NDVI junto con las 2 salidas de la segmentación)

+++

<img src="assets/img/S2_segment_results.png" width="70%">

@size[24px](Resultados de la segmentación)

+++

> @fa[tasks] **Tarea** 
>
> Ejecutar cualquiera de los 2 métodos de segmentación con diferentes parámetros y comparar los resultados

---

@snap[north span-100]
### Clasificación supervisada
@snapend

@snap[west span-55]
@ol[](false)
- digitalizar áreas de entrenamiento con [g.gui.iclass](http://grass.osgeo.org/grass76/manuals/g.gui.iclass.html) o [g.gui.vdigit](http://grass.osgeo.org/grass76/manuals/g.gui.vdigit.html) (recomendado)
- guardarlas en un mapa vectorial
@olend
@snapend

@snap[east span-45]
![g.gui.iclass](assets/img/g_gui_iclass.png)
@snapend

+++

Clasificación supervisada con Maximum Likelihood
@code[bash](code/04_S2_imagery_code.sh)

@[285-286](Convertir el vector de áreas de entrenamiento a raster)
@[288-290](Generar archivos de firma espectral)
@[292-294](Realizar la clasificación por ML)
@[296-301](Añadir etiquetas a las clases)

+++

@img[span-85](assets/img/sentinel_maxlik.png)

@size[24px](Clasificación supervisada con Maximum Likelihood)

+++

Clasificación supervisada con Machine Learning
@code[bash](code/04_S2_imagery_code.sh)

@[309-310](Instalar la extensión *r.learn.ml*)
@[312-314](Realizar la clasificación por RF)
@[316-321](Añadir etiquetas a las clases)

+++

@img[span-85](assets/img/sentinel_rf.png)

@size[24px](Clasificación supervisada con Random Forest)

+++

> @fa[tasks] **Tarea** 
>
> Comparar los resultados de ambos tipos de clasificación supervisada a través del índice Kappa

@snap[south span-50]
@fa[lightbulb text-orange] Hay un módulo [r.kappa](https://grass.osgeo.org/grass-stable/manuals/r.kappa.html)
<br><br>
@snapend

+++

### Post-procesamiento y validación

- [r.reclass.area](http://grass.osgeo.org/grass76/manuals/r.reclass.area.html) para eliminar pequeñas áreas, enmascarar nuevos valores y rellenar los huecos con [r.neighbors](http://grass.osgeo.org/grass76/manuals/r.neighbors.html) o [r.fillnulls](http://grass.osgeo.org/grass76/manuals/r.fillnulls.html)
- convertir la salida en vector y ejecutar [v.clean](http://grass.osgeo.org/grass76/manuals/v.clean.html) con tool=rmarea
- [r.kappa](https://grass.osgeo.org/grass76/manuals/r.kappa.html) para la validación (idealmente también digitalizar una muestra de prueba)

---

<img src="assets/img/gummy-question.png" width="45%">

---

**Gracias por su atención!!**

![GRASS GIS logo](assets/img/grass_logo_alphab.png)

---

@snap[north span-90]
<br><br><br>
Próxima presentación: 
<br>
[Object based image analysis - OBIA](https://gitpitch.com/veroandreo/maie-procesamiento/master?p=slides/05_obia&grs=gitlab)
@snapend

@snap[south span-50]
@size[18px](Presentation powered by)
<br>
<a href="https://gitpitch.com/">
<img src="assets/img/gitpitch_logo.png" width="20%"></a>
@snapend
