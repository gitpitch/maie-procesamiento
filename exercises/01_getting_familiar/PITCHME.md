---?image=assets/img/grass.png&position=bottom&size=100% 30%
@title[Front page]

@snap[north span-100]
<br>
## Procesamiento digital de imágenes satelitales y SIG
@snapend

@snap[south message-box-white]
Dra. Verónica Andreo<br>CONICET<br><br>
@img[width=600px](assets/img/LogoIG_CONAE_UNC.png)<br>
@snapend

---?image=assets/img/grass.png&position=bottom&size=100% 30%

## Ejercicio: Familiarizándonos con GRASS GIS

<br>

@fa[handshake fa-3x text-gray]

---

@snap[north-west span-60]
### Contenidos
@snapend

@snap[west span-100]
<br><br>
@ol[list-content-verbose]
- Estructura de la base de datos GRASS GIS
- Datos de muestra: *North Carolina sample location*
- Iniciamos GRASS y exploramos su interfaz gráfica
- Abrir mapas raster y vectoriales
- Consultas sobre mapas raster y vectoriales
- Visualización 3D
- Visualización de mapas base (WMS)
- Composición cartográfica
@olend
@snapend
---

Recuerdan los conceptos basicos? 

> - Location ?
> - Mapset ? 
> - Región computacional ?

@fa[grin fa-spin text-green fa-3x]

Pueden revisar nuevamente la [presentación introductoria](https://gitpitch.com/veroandreo/maie-procesamiento/master?p=slides/01_general_intro_grass&grs=gitlab#) para refrescar la memoria

---

@snap[north span-100]
### Datos de muestra: 
#### *North Carolina sample location*
@snapend

@snap[west span-60]
<br>
@ul[list-content-verbose](false)
- @fa[download text-green] [**North Carolina full**](https://grass.osgeo.org/sampledata/north_carolina/nc_spm_08_grass7.zip)
- Crear un directorio en *`$HOME`* (o dentro de Documentos) con el nombre *`grassdata`*
- Descomprimir el archivo *`nc_spm_08_grass7.zip`* dentro de *`grassdata`*
@ulend
@snapend

@snap[east span-40]
<br><br>
<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=-127.70507812500001%2C20.797201434307%2C-69.69726562500001%2C50.261253827584724&amp;layer=mapnik" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/#map=5/36.932/-98.701"></a></small>
@snapend

@snap[south span-100 text-07]
@fa[exclamation-triangle text-orange] Usuarios Windows: la descompresión crea un nivel extra de subdirectorios
@snapend
---

## Iniciamos GRASS GIS

<br>

- Click sobre el ícono de GRASS GIS (*MS Windows: Inicio @fa[arrow-right] OSGeo4W @fa[arrow-right] GRASS GIS*)
- Abrir una terminal o la *OSGeo4W Shell* y escribir:

<br>

```bash
# abrir GRASS con el GUI Location wizard
grass78

# abrir GRASS en modo texto, i.e., solo veremos la terminal
grass78 --text $HOME/grassdata/nc_spm_08_grass7/user1/
```

---

@snap[north span-100]
... y ahora qué?
<br>
@snapend

@snap[west span-55]
<img src="assets/img/start_screen3.png" width="92%">
@snapend

@snap[east span-45]
@ol[list-content-verbose]
1. Seleccionar la carpeta `grassdata`, la base de datos de GRASS
2. Seleccionar el location `nc_spm_08_grass7` 
3. Seleccionar el mapset `user1`
4. Click sobre `Start GRASS session`
@olend
@snapend

---

@snap[north span-100]
Si no han descargado el location de North Carolina (NC)... no hay problema!
@snapend

@snap[west span-50]
<br><br>
![Startup-download location](assets/img/download_location_button.png)
@snapend

@snap[east span-50]
![Download location](assets/img/download_location.png)
@snapend

---
@snap[north-west]
GRASS GIS GUI @fa[grin-alt text-green]
<br>
@snapend

<img src="assets/img/empty_gui_explained.png" width="88%">

---
@snap[north-west]
... y la Terminal @fa[grin-hearts text-green]
<br>
@snapend

<img src="assets/img/empty_terminal.png" width="80%">

---

### Obtener información sobre el CRS

<img src="assets/img/projection_info.png" width="65%">

<br>
o simplemente desde la terminal:

```bash
g.proj -p
```

---

### @fa[map text-green] Abrir mapas raster y vectoriales @fa[map text-green]

Muchas opciones:
- Desde el menú principal: File @fa[arrow-right] Map display @fa[arrow-right] Add raster|vector
- Desde los íconos de la barra de tareas del Layer Manager
- Desde la pestaña *Consola* del Layer Manager
- Doble-click sobre el mapa en la pestaña *Datos* del Layer Manager 
- Desde la terminal, llamando monitores wx: [*d.mon*](https://grass.osgeo.org/grass-stable/manuals/d.mon.html)

+++

> @fa[tasks] **Tarea** 
>
> Volver a revisar la presentación [Un paseo por las funciones de GRASS GIS](https://gitpitch.com/veroandreo/grass-gis-conae-es/master?p=slides/02_general_intro_capabilities&grs=gitlab#/10) y probar las diferentes maneras de abrir mapas en GRASS GIS

---

### Invocar comandos de GRASS GIS

- Desde la interfaz gráfica (GUI): 
  - menú principal del Layer Manager
  - pestaña *Console*
  - pestaña *Modules*
  
- Desde la terminal: 
  - tipear la primera letra o algunas letras del comando + `<tab><tab>` para opciones y autocompletado

+++

> @fa[tasks] **Tarea**
>
> - Ejecutar `r.univar map=elevation` desde la GUI (Menú Raster @fa[arrow-right] Reports and statistics)
> - Ejecutar `r.univar map=elevation` desde la pestaña *Console*
> - Escribir `r.un` en la terminal + `<tab><tab>`, luego `<Enter>`
> - Ejecutar `r.univar map=elevation` en la terminal

---

@snap[north span-80]
2 cosas para destacar en la GUI:
@snapend

@snap[west span-50]
<br>
<img src="assets/img/log_file_button.png" width="85%">
<br>
@size[24px](*Log file* y *Save* en la consola de la GUI)
@snapend

@snap[east span-50]
![Copy button](assets/img/copy_button.png)
<br>
@fa[copy] @size[24px](Botón *Copy* en la GUI de cada comando)
@snapend

---

### @fa[question-circle text-green] Ayuda @fa[question-circle text-green]

- Desde el menú principal `Help`
- En la GUI de cada comando
- `<comando> --help` en la terminal para obtener la descripción de parámetros y opciones
- `g.manual <comando>` para acceder al manual online

+++

> @fa[tasks] **Tarea** 
> 
> Obtener ayuda para `r.grow.distance` y `v.what.rast`. Para qué se usan? Cuáles son sus parámetros y opciones?

---

### Consultas sobre mapas raster

![Query raster map](assets/img/query_maps.png)

+++

### Consultas sobre mapas vectoriales

![Query vector map](assets/img/query_vector_maps.png)

+++

### Tabla(s) de atributos de mapas vectoriales

<img src="assets/img/vector_attr_table.png" width="85%">

+++

> @fa[tasks] **Tarea**
>
> - Abrir el mapa vectorial `zipcode`
> - Cambiar el color de las areas
> - Seleccionar sólo los límites y mostrarlos con otro color
> - Mostrar sólo los valores de `cat` entre 1 y 40
> - Construir una consulta SQL con al menos dos condiciones

---

### Explorando los datos de muestra y la región computacional
<br>

```bash
# lista de los mapas raster
g.list rast
# lista de los mapas vectoriales
g.list vect
# imprimir la región computacional
g.region -p
```

+++

> @fa[tasks] **Tareas**
>
>- Explorar la ayuda de *r.info* y *v.info* y obtener información básica sobre un mapa raster y un mapa vectorial
>- Cambiar la región computacional actual a un mapa vectorial e imprimir los nuevos ajustes
>- Alinear la resolución de la región computacional a un mapa raster e imprimir los nuevos ajustes para comprobar

---

### Visualización 3D

<img src="assets/img/3d_view.png" width="95%">

+++

> @fa[tasks] **Tareas**
>
> - Abrir el raster `elevation`
> - Cambiar a *Vista 3D* en la ventana *Map Display*
> - Explorar las opciones disponibles en la nueva pestaña 3D que aparece en el *Layer Manager*

---

@snap[north span-100]
### Visualizar mapas base WMS
@snapend

@snap[west span-50]
@size[26px](Paso 1)
<img src="assets/img/add_wms_1.png" width="95%">
@snapend

@snap[east span-50]
<br>
@size[26px](Paso 2)
<img src="assets/img/add_wms_2.png" width="85%">
@snapend

+++

@snap[north span-100]
### Visualizar mapas base WMS
@snapend

<img src="assets/img/add_wms_3.png" width="95%">

+++

> @fa[tasks] **Tarea**
>
> - Explorar el area, acercar y alejar
> - Abrir y mostrar un mapa vectorial sobre la capa WMS (**Sugerencia**: Ajustar la opacidad del mapa vectorial)

---

### Composición cartográfica en la GUI

> @fa[tasks] **Tarea**
>
>  - Desplegar los mapas `elevation` y `roadsmajor`
>  - Superponer una grilla
>  - Agregar *labels* a los caminos (**Sugerencia**: botón derecho sobre el nombre del mapa en el *Layer Manager*)
>  - Agregar las leyendas para el mapa raster y vectorial
>  - Agregar barra de escala
>  - Agregar símbolo indicando el *Norte*
>  - Agregar título al mapa

+++

<img src="assets/img/map_decorations_task.png" width="80%">

+++

> @fa[tasks] **Tarea** 
>
> Crear un mapa usando el compositor cartográfico ([g.gui.psmap](https://grass.osgeo.org/grass-stable/manuals/g.gui.psmap.html)) y guardar las instrucciones en un archivo .psmap.

---

<img src="assets/img/gummy-question.png" width="45%">

---

**Gracias por su atención!!**

![GRASS GIS logo](assets/img/grass_logo_alphab.png)

---

@snap[north span-90]
<br><br><br>
Próxima presentación: 
<br>
[Ejercicio: Crear un location e importar mapas a GRASS GIS](https://gitpitch.com/veroandreo/maie-procesamiento/master?p=exercises/02_create_new_location&grs=gitlab#/)
@snapend

@snap[south span-50]
@size[18px](Presentation powered by)
<br>
<a href="https://gitpitch.com/">
<img src="assets/img/gitpitch_logo.png" width="20%"></a>
@snapend

