---?image=assets/img/grass.png&position=bottom&size=100% 30%
@title[Front page]

@snap[north span-100]
<br>
## Procesamiento digital de imágenes satelitales y SIG
@snapend

@snap[south message-box-white]
Dra. Verónica Andreo<br>CONICET<br><br>
@img[width=600px](assets/img/LogoIG_CONAE_UNC.png)<br>
@snapend

---?image=assets/img/grass.png&position=bottom&size=100% 30%

## Ejercicio: Manos a la obra con series temporales de NDVI 

<br>

+++

@snap[north-west span-60]
### Contenidos
@snapend

@snap[west span-100]
<br><br>
@ol[list-content-verbose]
- Datos para el ejercicio
- Uso de bandas de confiabilidad
- Creación de serie de NDVI
- HANTS como método de reconstrucción
- Agregación temporal
- Índices de fenología
- NDWI y frecuencia de inundación
- Regresión entre NDVI y NDWI
@olend
@snapend

+++

@snap[north span-100]
### Datos para el ejercicio
@snapend

@snap[west span-40]
<br>
@ul[](false)
- Producto MODIS: <a href="https://lpdaac.usgs.gov/products/mod13c2v006/">MOD13C2 Collection 6</a>
- Composiciones globales mensuales 
- Resolución espacial: 5600m
- Mapset `modis_ndvi` 
@ulend
@snapend

@snap[east span-60]
@img[](assets/img/mod13c2_global_ndvi.png)
@snapend

+++

### @fa[download text-green] Código para el ejercicio @fa[download text-green]

<br>

- Descargar el [código](https://gitlab.com/veroandreo/maie-procesamiento/raw/master/code/05_ndvi_time_series_code.sh?inline=false) para seguir el ejercicio

---

### Familiarizarse con los datos de NDVI

@code[bash](code/05_ndvi_time_series_code.sh)

@[15-19](Iniciar GRASS GIS en el mapset *modis_ndvi*)
@[21-23](Agregar *modis_lst* a la lista de mapsets accesibles)
@[25-29](Listar los mapas y obtener información de alguno de ellos)

+++

> @fa[tasks] **Tarea**
> 
> - Mostrar los mapas de NDVI, NIR y pixel reliability.
> - Obtener información sobre los valores mínimos y máximos
> - ¿Qué se puede decir sobre los valores de cada banda?
> - ¿Hay valores nulos?

---

### Uso de la banda de confiabilidad

> @fa[tasks] **Tarea**
>
> - Leer acerca de la banda de confiabilidad en la [Guía de usuario](https://lpdaac.usgs.gov/documents/103/MOD13_User_Guide_V6.pdf) de MOD13 (pag 27).
> - Para una misma fecha mostrar la banda de confiabilidad y el NDVI.
> - Seleccionar sólo los pixeles con valor 0 (Buena calidad) en la banda de confiabilidad. ¿Qué se observa?

+++

### Uso de la banda de confiabilidad

@code[bash](code/05_ndvi_time_series_code.sh)

@[37-39](Definir la región computacional)
@[41-42](Establecer los límites provinciales como máscara)
@[44-49](Mantener sólo los pixeles de la mejor calidad - *nix)
@[51-55](Mantener sólo los pixeles de la mejor calidad - windows)
@[59-70](Mantener sólo los pixeles de la mejor calidad - para todos los mapas)

+++

> @fa[tasks] **Tarea**
>
> Como podrían hacer lo mismo pero con módulos temporales?

<br><br>
@snap[south-east span-100]
<br>
@img[width=90px](assets/img/tip.png) Que les parece [t.rast.algebra](https://grass.osgeo.org/grass-stable/manuals/t.rast.algebra.html)?
<br><br>
@snapend

+++

```bash
# apply pixel reliability band
t.rast.algebra \
  expression="NDVI_monthly_filt = if(pixel_rel_monthly != 0, null(), ndvi_monthly)"
  basename=ndvi_monthly \
  suffix=gran
```

+++

> @fa[tasks] **Tarea**
>
> Comparar las estadísticas entre los mapas de NDVI originales y filtrados para la misma fecha

---

### Creación de la serie de tiempo

@code[bash](code/05_ndvi_time_series_code.sh)

@[78-82](Crear STRDS de NDVI)
@[84-85](Corroborar que la STRDS fue creada)
@[87-88](Crear archivo con lista de mapas)
@[90-94](Asignar fecha a los mapas, i.e., registrar)
@[96-97](Imprimir info básica de la STRDS)
@[99-100](Imprimir la lista de mapas en la STRDS)

+++

> @fa[tasks] **Tarea**
> 
> Explorar visualmente los valores de las series temporales en diferentes puntos. 
> Usar [g.gui.tplot](https://grass.osgeo.org/grass-stable/manuals/g.gui.tplot.html) y seleccionar diferentes puntos interactivamente.

---

### Datos faltantes

@code[bash](code/05_ndvi_time_series_code.sh)

@[108-109](Obtener las estadísticas de la serie de tiempo)
@[111-114](Contar los datos válidos)
@[116-118](Estimar el porcentaje de datos faltantes)

+++

@snap[north span-100]
<br>
@img[width=90px](assets/img/tip.png) Cómo guardar en una variable el numero de mapas de una serie de tiempo?
<br><br>
@snapend

```bash
t.info -g ndvi_monthly
`eval t.info ndvi_monthly`
echo $number_of_maps

r.mapcalc \
  expression="ndvi_missing = (($number_of_maps - ndvi_count_valid) * 100.0)/$number_of_maps"
```

+++

> @fa[tasks] **Tarea**
>
> - Mostrar el mapa que representa el porcentaje de datos faltantes y explorar los valores. 
> - Obtener estadísticas univariadas de este mapa.
> - Donde estan los mayores porcentajes de datos faltantes? Por que creen que puede ser?

---

### Reconstrucción temporal: HANTS

- Harmonic Analysis of Time Series (HANTS)
- Implementado en la extensión [r.hants](https://grass.osgeo.org/grass7/manuals/addons/r.hants.html)

<img src="assets/img/evi_evi_hants.png" width="65%">

+++

### Reconstrucción temporal: HANTS

@code[bash](code/05_ndvi_time_series_code.sh)

@[126-127](Instalar la extensión *r.hants*)
@[129-135](Listar los mapas y aplicar r.hants - *nix)
@[137-141](Listar los mapas y aplicar r.hants - windows)

+++

> @fa[tasks] **Tarea**
>
> Probar diferentes ajustes de parámetros en [r.hants](https://grass.osgeo.org/grass7/manuals/addons/r.hants.html) y comparar los resultados

+++

### Reconstrucción temporal: HANTS

@code[bash](code/05_ndvi_time_series_code.sh)

@[143-148](Parcheo de serie original y reconstruída - *nix)
@[150-154](Parcheo de serie original y reconstruída - windows)
@[156-168](Parcheo de serie original y reconstruída)
@[169-174](Crear serie de tiempo con los datos parcheados)
@[175-183](Registrar los mapas en la serie de tiempo)
@[185-186](Imprimir información de la serie de tiempo)

+++

> @fa[tasks] **Tarea**
>
> - Evaluar gráficamente los resultados de la reconstrucción de HANTS en pixeles con mayor porcentaje de datos faltantes 
> - Obtener estadísticas univariadas para las nuevas series temporales

+++

> @fa[tasks] **Tarea**
>
> - Ver la sección de metodos en [Metz el al. 2017](https://www.mdpi.com/2072-4292/9/12/1333) 
> - Que otros algoritmos existen o que otra aproximación podria seguirse?

@img[span-60](https://www.mdpi.com/remotesensing/remotesensing-09-01333/article_deploy/html/images/remotesensing-09-01333-ag.png)

---

### Agregación con granularidad

<br>

> @fa[tasks] **Tarea**
>
> - Obtener el promedio de NDVI cada dos meses
> - Visualizar la serie de tiempo resultante con [g.gui.animation](https://grass.osgeo.org/grass-stable/manuals/g.gui.animation.html)

---

### Fenología

@code[bash](code/05_ndvi_time_series_code.sh)

@[194-198](Mes de máximo y mes de mínimo)
@[200-209](Reemplazar con *start_month()* los valores en la STRDS si coinciden con el mínimo o máximo global)
@[211-215](Obtener el primer mes en el que aparecieron el máximo y el mínimo)
@[217-218](Eliminar la serie temporal intermedia)

+++

> @fa[tasks] **Tarea**
>
> - Mostrar los mapas resultantes con [g.gui.mapswipe](https://grass.osgeo.org/grass-stable/manuals/g.gui.mapswipe.html)
> - Cuando se observan los minimos y maximos? Hay algun patron? A que se podria deber?

+++

> @fa[tasks] **Tarea**
>
> Asociar el máximo de LST con el máximo de NDVI y, la fecha de la máxima LST con la fecha del máximo NDVI

<br>
@snap[south-east span-70]
@img[width=90px](assets/img/tip.png) Ver el módulo [r.covar](https://grass.osgeo.org/grass-stable/manuals/r.covar.html)
<br><br>
@snapend

+++

### Fenología

@code[bash](code/05_ndvi_time_series_code.sh)

@[220-223](Obtener series temporales de pendientes entre mapas consecutivos)
@[225-229](Obtener la máxima pendiente por año)

+++

> @fa[tasks] **Tarea**
>
> - Obtener un mapa con la mayor tasa de crecimiento por píxel en el período 2015-2019
> - Que modulo usarían?

+++

### Fenología

@code[bash](code/05_ndvi_time_series_code.sh)

@[231-232](Instalar la extensión *r.seasons*)
@[234-238](Determinar el comienzo, el final y la duración del período de crecimiento - *nix)
@[240-243](Determinar el comienzo, el final y la duración del período de crecimiento - windows)

+++

> @fa[tasks] **Tarea**
>
> - Que nos dice cada mapa? Donde es mas larga la estacion de crecimiento?
> - Exportar los mapas resultantes como .png

+++

### Fenología

@code[bash](code/05_ndvi_time_series_code.sh)

@[245-246](Crear un mapa de umbrales para usar en *r.seasons*)

+++

> @fa[tasks] **Tarea**
>
> Utilizar el mapa de umbrales en [r.seasons](https://grass.osgeo.org/grass7/manuals/addons/r.seasons.html) y comparar los mapas de salida con los resultados de utilizar sólo un valor de umbral

---

### Serie de tiempo de NDWI

@code[bash](code/05_ndvi_time_series_code.sh)

@[254-263](Crear series temporales de NIR y MIR)
@[265-267](Listar archivos NIR y MIR)
@[269-276](Registrar mapas)
@[278-280](Imprimir información de la serie de tiempo)
@[282-285](Estimación de la serie temporal de NDWI)

+++

> @fa[tasks] **Tarea**
>
> Obtener valores máximos y mínimos para cada mapa de NDWI y explorar el trazado de la serie de tiempo en diferentes puntos de forma interactiva

<br><br>

@snap[south-east span-70]
@img[width=90px](assets/img/tip.png) Ver el manual de [t.rast.univar](https://grass.osgeo.org/grass-stable/manuals/t.rast.univar.html)
<br><br>
@snapend

---

### Frecuencia de inundación

@code[bash](code/05_ndvi_time_series_code.sh)

@[292-295](Reclasificar los mapas según un umbral)
@[297-298](Obtener frecuencia de inundación)

+++

> @fa[tasks] **Tarea**
>
> Cuáles son las áreas que se han inundado con más frecuencia?

---

### Regresión

@code[bash](code/05_ndvi_time_series_code.sh)

@[306-307](Instalar la extensión *r.regression.series*)
@[309-316](Realizar una regresión entre las series temporales de NDVI y NDWI - *nix)
@[318-322](Realizar una regresión entre las series temporales de NDVI y NDWI - windows)

+++

> @fa[tasks] **Tarea**
>
> Determinar dónde está la mayor correlación entre NDVI y NDWI

---

<img src="assets/img/gummy-question.png" width="45%">

---

**Gracias por su atención!!**

![GRASS GIS logo](assets/img/grass_logo_alphab.png)

---

@snap[north span-90]
<br><br><br>
Próxima presentación: 
<br>
[Analisis de series de tiempo con BFAST](https://gitpitch.com/veroandreo/maie-procesamiento/master?p=slides/07_bfast&grs=gitlab#/)
@snapend

<br><br>
@snap[south span-50]
@size[18px](Presentation powered by)
<br>
<a href="https://gitpitch.com/">
<img src="assets/img/gitpitch_logo.png" width="20%"></a>
@snapend
