---?image=assets/img/grass.png&position=bottom&size=100% 30%
@title[Front page]

@snap[north span-100]
<br>
## Procesamiento digital de imágenes satelitales y SIG
@snapend

@snap[south message-box-white]
Dra. Verónica Andreo<br>CONICET<br><br>
@img[width=600px](assets/img/LogoIG_CONAE_UNC.png)<br>
@snapend

---?image=assets/img/grass.png&position=bottom&size=100% 30%

## Procesamiento de datos satelitales en GRASS GIS

<br>

---

@snap[north-west span-60]
<h3>Contenidos</h3>
@snapend

@snap[west span-100]
<br><br>
@ol[list-content-verbose]
- Nociones básicas sobre datos satelitales en GRASS GIS
- De número digital a reflectancia
- Fusión de datos/Pansharpening
- Composiciones RGB
- Máscara de nubes a partir de banda de calidad
- Índices espectrales de agua y vegetación
- Clasificación no supervisada
@olend
@snapend

---

### Nociones básicas sobre datos satelitales en GRASS GIS

Los datos satelitales en general vienen en formato raster @fa[arrow-right text-green] aplican las mismas reglas
<br>

> Los comandos @color[#8EA33B](*i.**) se orientan explícitamente al procesamiento de datos satelitales (aunque algunos puedan usarse para otros datos raster)

@size[24px](Para más detalles ver el manual <a href="https://grass.osgeo.org/grass-stable/manuals/imageryintro.html">Imagery Intro</a> y la wiki <a href="https://grasswiki.osgeo.org/wiki/Image_processing">Image Processing</a>)

---

@snap[north span-100]
### Datos
@snapend

@snap[west span-50]
Escena Landsat 8 (OLI)
@ul[list-content-verbose](false)
- Fecha: 16 de Junio de 2016
- Path/Row: 015/035
- CRS: UTM zona 18 N (EPSG:32618)
@ulend
<br><br>
@snapend

@snap[east span-50]
![L8](https://landsat.gsfc.nasa.gov/wp-content/uploads/2013/01/ldcm_2012_COL.png)
<br><br>
@snapend

@snap[south span-100 text-09]
@fa[download text-green] Descargar la escena [Landsat 8](https://gitlab.com/veroandreo/curso-grass-gis-rioiv/raw/master/data/NC_L8_scenes.zip?inline=false) ya recortada, y descomprimirla dentro de `$HOME/gisdata`.
<br>
@fa[download text-green] Descargar el [código](https://gitlab.com/veroandreo/grass-gis-conae/raw/master/code/04_L8_imagery_code.sh?inline=false) para esta sesión.
<br><br>
@snapend

+++

Historia de la mision Landsat

@img[span-85](assets/img/Landsat_history.jpg)

+++

![L8 vs L7 bands](https://landsat.gsfc.nasa.gov/wp-content/uploads/2013/01/ETM+vOLI-TIRS-web_Feb20131.jpg)

@size[24px](Comparación entre las bandas de Landsat 7 ETM+ y Landsat 8 OLI. Fuente: <https://landsat.gsfc.nasa.gov/landsat-data-continuity-mission/> y <a href="https://landsat.usgs.gov/what-are-band-designations-landsat-satellites">detalle de las bandas Landsat</a>)

---

Iniciar GRASS GIS y crear un nuevo mapset
@code[bash](code/04_L8_imagery_code.sh)

@[19-20](Iniciar GRASS GIS en el location NC y crear un nuevo mapset llamado *landsat8*)
@[21-22](Corroborar la proyeccion)
@[23-28](Listar los mapsets y agregar el mapset *landsat* a la lista de mapsets accesibles)
@[29-30](Listar los mapas raster disponibles)
@[31-32](Establecer la región computacional a una escena landsat)
   
---
Importar los datos L8
@code[bash](code/04_L8_imagery_code.sh)

@[34-40](Cambiar de directorio y definir variable)
@[48-59](Importar todas las bandas con 30m de res, notar `extent=region`)
@[61-69](Importar la banda pancromática con 15m de res)            

+++

@snap[north span-100]
Opción *Directorio* para importar desde la GUI
@snapend

@snap[west span-50]
<br>
<img src="assets/img/import_directory_1.png">
@snapend

@snap[east span-50]
<br>
<img src="assets/img/import_directory_2.png">
@snapend

---

### Pre-procesamiento de datos satelitales

<br>
![Workflow](assets/img/rs_workflow.jpg)

---

### De número digital (DN) a reflectancia y temperatura

- Los datos L8 OLI vienen en 16-bit con rango de datos entre 0 y 65535.
- [i.landsat.toar](https://grass.osgeo.org/grass-stable/manuals/i.landsat.toar.html) convierte DN en reflectancia TOA (y temperatura de brillo) para todos los sensores Landsat. Opcionalmente proporciona reflectancia de superficie (BOA) después de la corrección DOS. 
- [i.atcorr](https://grass.osgeo.org/grass-stable/manuals/i.atcorr.html) proporciona un método de corrección atmosférica más complejo para muchos sensores (S6).

+++?code=code/04_L8_imagery_code.sh&lang=bash&title=DN a Reflectancia y Temperatura de Superficie

@[77-79](Convertir DN a reflectancia superficial y temperatura - método DOS)
@[81-85](Corroborar info antes y después de la conversión para una banda)

+++

![Band 10 Temperature](assets/img/L8_band10_kelvin.png)

@size[24px](Banda 10 de L8 con la paleta de colores *kelvin*)

---

### Fusión de datos/Pansharpening
<br>
Vamos a usar la banda PAN (15m) para mejorar la definición de las bandas espectrales de 30m, por medio de:

> [i.fusion.hpf](https://grass.osgeo.org/grass7/manuals/addons/i.fusion.hpf.html), que aplica un método de adición basado en un filtro de paso alto

Otros métodos están implementados en [i.pansharpen](https://grass.osgeo.org/grass-stable/manuals/i.pansharpen.html)

+++

Fusión de datos/Pansharpening
@code[bash](code/04_L8_imagery_code.sh)

@[93-94](Instalar la extensión *i.fusion.hpf*)
@[96-97](Cambiar la región a la banda PAN)
@[99-104](Ejecutar la fusión)
@[106-107](Listar los mapas resultantes usando un patrón de busqueda)
@[109-111](Visualizar las diferencias)

+++            

<img src="assets/img/pansharpen_mapswipe.png" width="75%">

@size[24px](Datos originales 30m y datos fusionados 15m)

---?code=code/04_L8_imagery_code.sh&lang=bash&title=Composiciones

@[119-123](Mejorar los colores para una composición RGB color natural)
@[125-129](Mostrar la combinación RGB - d.rgb)
@[131-135](Crear una composición RGB 432)
@[137-141](Mejorar los colores para una composición RGB en falso color)
@[143-147](Crear una composición RGB 543)
@[149-151](Mostrar la composición RGB 543)

+++

![Composites 432 and 543](assets/img/composites_432_543.png)

@size[24px](Composiciones color natural 432 y falso color 543 de la imagen Landsat 8 del 16 de Junio de 2016)

---

### Enmascarado de nubes a partir de la banda de calidad QA

- Landsat 8 proporciona una banda de calidad (QA) con valores enteros de 16 bits que representan las combinaciones de superficie, atmósfera y condiciones del sensor que pueden afectar la utilidad general de un determinado pixel. 
- La extensión [i.landsat8.qc](https://grass.osgeo.org/grass7/manuals/addons/i.landsat8.qc.html) reclasifica la banda QA de Landsat 8 de acuerdo a la calidad del pixel. 

<br>
@size[24px](Para más información sobre la banda QA de L8, ver: http://landsat.usgs.gov/qualityband.php)

+++

Aplicar máscara de nubes
@code[bash](code/04_L8_imagery_code.sh)
 
@[159-160](Instalar la extensión *i.landsat8.qc*)
@[162-163](Crear las reglas para identificar las nubes)
@[165-166](Reclasificar la banda QA en función de las reglas)
@[168-169](Reportar el área cubierta por nubes)
@[171-173](Mostrar el mapa reclasificado)

+++

> @fa[tasks] **Tarea:** Comparar visualmente la cobertura de nubes con la composición RGB 543.

+++

![Cloud mask and Composites 543](assets/img/cloud_composite_543.png)

@size[24px](Composición falso color y máscara de nubes para la imagen Landsat 8 del 16 de Junio de 2016)


---

### Índices de agua y vegetación
@code[bash](code/04_L8_imagery_code.sh)

@[181-182](Establecer la máscara de nubes para evitar la computación sobre las nubes)
@[184-187](Calcular el NDVI y establecer la paleta de colores)
@[189-192](Calcular NDWI y establecer la paleta de colores)
@[194-195](Quitar la máscara)
@[197-200](Mostrar los mapas)

+++

![NDVI and NDWI](assets/img/L8_ndvi_ndwi.png)

@size[24px](NDVI y NDWI a partir de datos Landsat 8 del 16 de Junio de 2016)

---

### Clasificación No Supervisada

@ol[]
- Agrupar las bandas (i.e., hacer un stack): [i.group](https://grass.osgeo.org/grass-stable/manuals/i.group.html)
- Generar firmas para *n* número de clases: [i.cluster](https://grass.osgeo.org/grass-stable/manuals/i.cluster.html)
- Clasificar: [i.maxlik](https://grass.osgeo.org/grass-stable/manuals/i.maxlik.html)
@olend   
        
+++

Clasificación No Supervisada
@code[bash](code/04_L8_imagery_code.sh)

@[208-209](Listar los mapas usando un patrón)
@[211-213](Crear un grupo o *stack*)
@[215-219](Obtener estadísticos -firmas- para las *n* clases de interés con una muestra de pixeles)
@[221-225](Realizar la clasificación no supervisada de toda la imagen)
@[227-229](Mostrar el mapa clasificado)

+++

![L8 Unsupervised Classification](assets/img/L8_unsup_class.png)

@size[24px](Clasificación No Supervisada - imagen Landsat 8 del 16 de Junio de 2016)

+++

Información derivada adicional podría obtenerse con los siguientes módulos, entre otros:

- medidas de textura: [r.texture](https://grass.osgeo.org/grass-stable/manuals/r.texture.html), 
- medidas de diversidad: [r.diversity](https://grass.osgeo.org/grass7/manuals/addons/r.diversity.html), 
- estadísticas locales con información de contexto: [r.neighbors](https://grass.osgeo.org/grass-stable/manuals/r.neighbors.html),
- etc.

---

### @fa[bookmark text-green] Clasificación en GRASS GIS @fa[bookmark text-green]

- [Topic classification](http://grass.osgeo.org/grass-stable/manuals/topic_classification.html) en los manuales de GRASS GIS
- [Image classification](http://grasswiki.osgeo.org/wiki/Image_classification) en la wiki
- [Ejemplos de clasificacion](http://training.gismentors.eu/grass-gis-irsae-winter-course-2018/units/28.html) en el curso dictado en Noruega en 2018
- [Clasificacion con Random Forest](https://neteler.gitlab.io/grass-gis-analysis/03_grass-gis_ecad_randomforest/) en la presentacion del OpenGeoHub Summer School 2018 en Praga

---

<img src="assets/img/gummy-question.png" width="45%">

---

**Gracias por su atención!!**

![GRASS GIS logo](assets/img/grass_logo_alphab.png)

---

@snap[north span-90]
<br><br><br>
Próxima presentación: 
<br>
[Ejercicio: Trabajamos con imágenes Sentinel 2](https://gitpitch.com/veroandreo/maie-procesamiento/master?p=exercises/03_processing_sentinel2&grs=gitlab#/)
@snapend

@snap[south span-50]
@size[18px](Presentation powered by)
<br>
<a href="https://gitpitch.com/">
<img src="assets/img/gitpitch_logo.png" width="20%"></a>
@snapend
