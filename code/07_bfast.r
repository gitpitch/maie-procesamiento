#########################################################################
# Commands for the BFAST lecture for Procesamiento - MAIE. IG.
# Author: Veronica Andreo
# Date: August, 2020
# Adapted from: http://www.loicdutrieux.net/bfastSpatial/
#########################################################################

# Load libraries (install them beforehand)
library(rgrass7)
library(spacetime)
library(raster)
library(rgdal)
library(bfast)
library(devtools)
install_github('loicdtx/bfastSpatial') # pkg not in CRAN
library(bfastSpatial)
library(mapview)
library(tmap)

# Import vector map
use_sf()
cba <- readVECT("provincia_cba")

# Import ndvi strds into R
ndvi <- read.tgrass("/home/veroandreo/ndvi_monthly.tar.gzip", 
                   localName = FALSE, useTempDir = FALSE)

# Get information about the RasterStack
class(ndvi)
dim(ndvi)

# Plot a layer of the rasterStack
mapview(ndvi[[3]], na.color = NA)

# Convert to brick
ndvi_b <- brick(ndvi)

# Assign dates to brick layers
ndvi_b <- setZ(ndvi_b, getZ(ndvi))

# Check date in a brick's layer
ndvi_b[[2]]@z

# Get information about the RasterBrick
class(ndvi_b)
dim(ndvi_b)

# Plot a layer of the rasterBrick
mapview(ndvi_b[[3]], na.color = NA)

# Animation
animate(ndvi_b, pause = 0.5, n = 1)

# Plot the time series of a pixel
pixel <- 1269

# Create ts object
ndvi_ts <- ts(as.vector(ndvi_b[pixel]), 
              frequency = 12, 
              start = c(2015, 1), 
              end=c(2020, 1))

# Plot
plot(ndvi_ts, ylab="NDVI")

# time series objects with zoo
# ndvi_zoo <- zoo(as.vector(ndvi_b[pixel]),ndvi_b@z$time)
# plot(ndvi_zoo)

# Run bfast for a single time series
bfast_ndvi_ts <- bfast(ndvi_ts, 
                       h = 0.1, 
                       season = "harmonic", 
                       max.iter = 10, 
                       breaks = 1)

# Plot bfast results
plot(bfast_ndvi_ts)
plot(bfast_ndvi_ts, type = "all")

# Run bfast monitor on single pixels
bfm <- bfmPixel(ndvi_b, 
                cell = pixel, 
                start = c(2017, 1), 
                plot = TRUE)

# Inspect object
bfm

# Test with different configurations and different pixels
bfm1 <- bfmPixel(ndvi_b, 
                 cell = pixel, 
                 start = c(2017, 1), 
                 formula = response~harmon, 
                 plot = TRUE)

bfm2 <- bfmPixel(ndvi_b, 
                 cell = pixel, 
                 start = c(2017, 1), 
                 formula = response~harmon, 
                 order = 3, plot = TRUE)

bfm3 <- bfmPixel(ndvi_b, 
                 cell = pixel, 
                 start = c(2017, 1), 
                 formula = response~trend, 
                 plot=TRUE)

# Run bfast monitor for the spacetime dataset
bfm_sp <- bfmSpatial(ndvi_b, 
                     start = c(2017, 1), 
                     order = 2)

# Inspect object
bfm_sp

# Plot
plot(bfm_sp)

# Extract change raster
change <- raster(bfm_sp, 1)

# Convert to months
months <- changeMonth(change)

# Set up labels and colormap for months
monthlabs <- c("jan", "feb", "mar", "apr", 
               "may", "jun", "jul", "aug",
               "sep", "oct", "nov", "dec")

# Plot
tm_shape(months) + 
  tm_raster(labels = monthlabs,
            style = "cat",
            title = "Month") +
  tm_shape(cba) +
  tm_borders() +
  tm_layout(panel.labels = c("2017", "2018", "2019"))

# Extract and rescale magnitude raster
magn <- raster(bfm_sp, 2) / 10000

# Make a copy showing only breakpoint pixels
magn_bkp <- magn
magn_bkp[is.na(change)] <- NA

# Stack
magnitude <- stack(c(magn, magn_bkp))

# Plot
tm_shape(magnitude) + 
  tm_raster(style = "cont",
            palette = "-YlOrRd",
            title = "Magnitude") +
  tm_shape(cba) +
  tm_borders() +
  tm_layout(panel.labels = c("Magnitude: all",
                             "Magnitude: bkp"))

# Apply a -1000 threshold
magnthresh <- magn_bkp
magnthresh[magnthresh > -0.1] <- NA

# Stack
magnitude <- stack(c(magn_bkp, magnthresh))

# Plot
tm_shape(magnitude) + 
  tm_raster(style = "cont",
            palette = "-YlOrRd",
            title = "Magnitude") +
  tm_shape(cba) +
  tm_borders() +
  tm_layout(panel.labels = c("Magnitude: bkp",
                             "Magnitude < -0.1"))

# Remove areas smaller than 3 pixels
magn_sieve <- areaSieve(magnthresh, thresh=90000000)
magn_as_rook <- areaSieve(magnthresh, thresh=90000000, directions=4)

# Stack
magnitude <- stack(c(magnthresh, magn_sieve, magn_as_rook))

# Plot
tm_shape(magnitude) + 
  tm_raster(style = "cont",
            palette = "-YlOrRd",
            title = "Magnitude") +
  tm_shape(cba) +
  tm_borders() +
  tm_layout(panel.labels = c("Magnitude < -0.1",
                             "Magnitude sieve",
                             "Magnitude rook"))

# Clump patches
changeSize_queen <- clumpSize(magn_sieve)
changeSize_rook <- clumpSize(magn_as_rook, directions=4)

# Stack
changesize <- stack(c(changeSize_queen, changeSize_rook))

# Plot
tm_shape(changesize) + 
  tm_raster(style = "cont",
            palette = "plasma",
            title = "Size") +
  tm_shape(cba) +
  tm_borders() +
  tm_layout(panel.labels = c("Size Queen",
                             "Size Rook"))

# Clump size in ha
changeSize <- clumpSize(magn_sieve, f=30000000/10000)

# Plot
tm_shape(changeSize) + 
  tm_raster(style = "cont",
            palette = "plasma",
            title = "Size (has)") +
  tm_layout(legend.outside = TRUE) +
  tm_shape(cba) +
  tm_borders()

# Print stats
changeSize <- clumpSize(magn_sieve, f=30000000/10000, stats=TRUE)
print(changeSize$stats)

# Export a raster map back to GRASS
writeRAST(changeSize, "change_size")
